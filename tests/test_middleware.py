from django import VERSION as DJ_VERSION
from django.test import SimpleTestCase, RequestFactory, override_settings

from .django_settings import *

from amp_tools.middleware import AMPDetectionMiddleware
from amp_tools.settings import defaults as default_settings


class AMPDetectionMiddlewareTest(SimpleTestCase):
    def setUp(self) -> None:
        self.factory = RequestFactory()
        self.middleware = AMPDetectionMiddleware(get_response=lambda x: x)
        self.default_amp_get = {
            default_settings.AMP_TOOLS_GET_PARAMETER: default_settings.AMP_TOOLS_GET_VALUE
        }

    def call_middleware(self, request):
        if DJ_VERSION[0] > 1:
            self.middleware(request)
        else:
            self.middleware.process_request(request)

    def test_through_get_param_false(self) -> None:
        request = self.factory.get("/")
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, False)

    def test_through_get_param_true(self) -> None:
        request = self.factory.get("/", self.default_amp_get)
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, True)

    @override_settings(
        AMP_TOOLS_GET_PARAMETER="custom_amp_param",
        AMP_TOOLS_GET_VALUE="custom_amp_value",
    )
    def test_through_custom_get_param_true(self) -> None:
        request = self.factory.get("/", {"custom_amp_param": "custom_amp_value"})
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, True)

    @override_settings(
        AMP_TOOLS_GET_PARAMETER="custom_amp_param",
        AMP_TOOLS_GET_VALUE="custom_amp_value",
    )
    def test_through_custom_get_param_false(self) -> None:
        request = self.factory.get("/", self.default_amp_get)
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, False)

    @override_settings(AMP_TOOLS_ACTIVE_URLS=["^/$"])
    def test_active_urls_allowed(self):
        # TODO Document or remove AMP_TOOLS_ACTIVE_URLS
        request = self.factory.get("/", self.default_amp_get)
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, True)

    @override_settings(AMP_TOOLS_ACTIVE_URLS=["^/$"])
    def test_active_urls_denied(self):
        # TODO Document or remove AMP_TOOLS_ACTIVE_URLS
        request = self.factory.get("/denied-url", self.default_amp_get)
        self.call_middleware(request)
        self.assertEqual(request.is_amp_detect, False)
