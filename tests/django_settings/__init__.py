import os
import django
from django.conf import settings

if not settings.configured:
    TEST_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    settings.configure(
        INSTALLED_APPS=("django.contrib.sites", "amp_tools", "tests"),
        TEMPLATES=[
            {
                "BACKEND": "django.template.backends.django.DjangoTemplates",
                "OPTIONS": {
                    "context_processors": [
                        "django.template.context_processors.request",
                    ],
                    "loaders": [
                        "amp_tools.loader.Loader",
                        "django.template.loaders.filesystem.Loader",
                        "django.template.loaders.app_directories.Loader",
                    ],
                },
            }
        ],
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": os.path.join(TEST_DIR, "test.sqlite3"),
            }
        },
        SITE_ID=1,
    )

    django.setup()
