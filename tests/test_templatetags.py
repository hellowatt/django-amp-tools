from unittest import skip

from django.template import Template, RequestContext, Context
from django.test import TestCase, RequestFactory

from amp_tools.templatetags.amp_tags import amp_img, amp_safe
from .django_settings import *

from amp_tools.settings import defaults as default_settings


class TemplateTagsTest(TestCase):
    # TODO Document template tags
    def setUp(self) -> None:
        self.factory = RequestFactory()

    def assertTemplate(self, template_str, expectedHTML, request=None):
        template = Template(template_str)
        request = request or self.factory.get("/", {})
        context = RequestContext(request, {})
        rendered = template.render(context)
        self.assertHTMLEqual(rendered, expectedHTML)

    def test_amp_canonical_link_amp(self):
        request = self.factory.get("/my_url", {})
        self.assertTemplate(
            "{% load amp_tags %}{% amp_canonical_link request %}",
            f'<link href="http://mysite.com/my_url?{default_settings.AMP_TOOLS_GET_PARAMETER}={default_settings.AMP_TOOLS_GET_VALUE}" rel="amphtml">',
            request=request,
        )

    def test_amp_canonical_link_canonical(self):
        request = self.factory.get(
            "/my_url",
            {
                default_settings.AMP_TOOLS_GET_PARAMETER: default_settings.AMP_TOOLS_GET_VALUE
            },
        )
        self.assertTemplate(
            "{% load amp_tags %}{% amp_canonical_link request %}",
            f'<link href="http://mysite.com/my_url" rel="canonical">',
            request=request,
        )

    def test_amp_canonical_link_canonical_no_request(self):
        template = Template("{% load amp_tags %}{% amp_canonical_link request %}")
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, "")

    def test_amp_canonical_link_encodeamp(self):
        request = self.factory.get("/my_url?param=value", {})
        self.assertTemplate(
            "{% load amp_tags %}{% amp_canonical_link request %}",
            f'<link href="http://mysite.com/my_url?param=value&amp;{default_settings.AMP_TOOLS_GET_PARAMETER}={default_settings.AMP_TOOLS_GET_VALUE}" rel="amphtml">',
            request=request,
        )

    def test_amp_link(self):
        self.assertTemplate(
            '{% load amp_tags %}{% amp_link "/path/" %}',
            f"/path/?{default_settings.AMP_TOOLS_GET_PARAMETER}={default_settings.AMP_TOOLS_GET_VALUE}",
        )

    def test_amp_urlparam(self):
        self.assertTemplate(
            '{% load amp_tags %}{{ "/path/"|amp_urlparam }}',
            f"/path/?{default_settings.AMP_TOOLS_GET_PARAMETER}={default_settings.AMP_TOOLS_GET_VALUE}",
        )

    def test_amp_img(self):
        html_content = """
                    <html><body>
                        <img alt="alternate text" src="/media/uploads/img.png" width="800" height="600" style="width: 100%;">
                        <img alt="alternate text2" src="/media/uploads/img2.png" style="width: 100%;" />
                    </body></html>
                """
        amp_content = amp_img(html_content)
        self.assertHTMLEqual(
            amp_content,
            """
            <html><body>
                <amp-img alt="alternate text" src="/media/uploads/img.png" width="800" height="600" style="width: 100%;" layout="responsive"></amp-img>
                <amp-img alt="alternate text2" src="/media/uploads/img2.png" style="width: 100%;"  layout="responsive" width="1.33" height="1"></amp-img>
            </body></html>
        """,
        )

    def test_amp_safe(self):
        html_content = """
            <html><body>
                <img alt="alternate text" src="/media/uploads/img.png" width="800" height="600" style="width: 100%;">
                <img alt="alternate text2" src="/media/uploads/img2.png" style="width: 100%;" />
            </body></html>
        """
        amp_content = amp_safe(html_content)
        self.assertHTMLEqual(
            amp_content,
            """
                    <html><body>
                        <amp-img alt="alternate text" src="/media/uploads/img.png" width="800" height="600"  layout="responsive"></amp-img>
                        <amp-img alt="alternate text2" src="/media/uploads/img2.png"   layout="responsive" width="1.33" height="1"></amp-img>
                    </body></html>
                """,
        )
