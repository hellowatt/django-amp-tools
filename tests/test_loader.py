from django.template.loader import get_template
from django.test import SimpleTestCase, override_settings

from .django_settings import *

from amp_tools.loader import Loader as AmpLoader
from amp_tools import set_amp_detect


class LoaderTest(SimpleTestCase):
    def setUp(self) -> None:
        set_amp_detect(True)

    def test_loader(self):
        template = get_template('default.html')
        self.assertTrue(True) # If we reach that statement it means a template was found

    def test_loader_deep(self):
        template = get_template('deep/default_deep.html')
        self.assertTrue(True) # If we reach that statement it means a template was found

    @override_settings(
        AMP_TOOLS_TEMPLATE_FOLDER="custom_amp"
    )
    def test_custom_folder(self):
        # TODO Document this feature
        template = get_template('custom.html')
        self.assertTrue(True) # If we reach that statement it means a template was found

    @override_settings(
        AMP_TOOLS_TEMPLATE_FOLDER="custom_amp"
    )
    def test_custom_folder_deep(self):
        template = get_template('deep/custom_deep.html')
        self.assertTrue(True) # If we reach that statement it means a template was found

    @override_settings(
        AMP_TOOLS_TEMPLATE_PREFIX="custom_amp"
    )
    def _test_amp_tools_template_prefix(self):
        # TODO document or fix this feature
        # TODO Figure out a use case for this. The prefix is always added, amp or not
        pass
